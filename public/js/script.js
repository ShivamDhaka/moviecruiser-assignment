var movies = [];

function getMovies() {

	var xhr = new XMLHttpRequest();

	xhr.open("get", "http://localhost:3000/movies");
	xhr.send(null)

	xhr.onload = function() {
    	movies = JSON.parse(xhr.responseText);
    	console.log(movies);
    	for(let i=0;i<movies.length;i++) {
        	document.getElementById("moviesList").innerHTML += 
			`<div class="card">
			<u><h5>${movies[i].name}</h5></u><br>
			<img src=${movies[i].image} alt="${movies[i].name}"><br><br>
			<button type="button" class="btn btn-primary" onclick="addFavourite(${movies[i].id})">Add to Favourite</button>
			</div><br>`
    	}
	}
}

function getFavourites() {
	var movies = [];

	var xhr = new XMLHttpRequest();
	console.log(xhr.readyState);

	xhr.open("get", "http://localhost:3000/favourites");
	xhr.send(null)

	xhr.onload = function() {
    	favourite = JSON.parse(xhr.responseText);
    	console.log(favourite);
    	for(let i=0;i<favourite.length;i++) {
        	document.getElementById("favouritesList").innerHTML += `
			<div class="card">
			<u><h5>${favourite[i].name}</h5></u><br>
			<img src="${favourite[i].image}" alt="${favourite[i].name}"><br><br>
			<button type="button" class="btn btn-primary" onclick="deleteFavourites(${favourite[i].id})">Delete Movie</button><br>
			</div>`
    	}
	}
}

function addFavourite(favId) {
	var fav = [];
	console.log("Added to favourite");
	let obj = getMovieInfoById(favId)
	fav.push(obj);
	console.log(fav);

	return fetch("http://localhost:3000/favourites/",
	{
		method:'POST',
		body: JSON.stringify(obj),
		headers: {
			"Content-Type":"application/json"
		}
	})
	.then(res => res.json())
	.then(console.log(favId))
}

const getMovieInfoById = (movieId) => {
	for(let i=0;i<movies.length;i++) {
		if(movieId == movies[i].id) {
			console.log(movies[i]);
			return movies[i];
		}
	}
}


const deleteFavourites = (favId) => {
	console.log(`Movie id = ${favId} deleted`);
	return fetch(`http://localhost:3000/favourites/${favId}`,
	{
		method:'DELETE',
		headers: {"Content-type" : "application/json"}
	})
}

module.exports = {
	getMovies,
	getFavourites,
	addFavourite,
	deleteFavourites
};

// You will get error - Uncaught ReferenceError: module is not defined
// while running this script on browser which you shall ignore
// as this is required for testing purposes and shall not hinder
// it's normal execution


